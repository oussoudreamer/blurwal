"""
Test cases for the environment module's preparation and backend retrieval.

Author: Benedikt Vollmerhaus
License: MIT
"""

from unittest.mock import create_autospec

import pytest

from blurwal import environment, paths
from blurwal.backends import base


@pytest.fixture(autouse=True)
def patch_backends(mocker):
    """
    Replace module constants for backends and desktop name mappings
    with mock backends to test without requiring any implementations.
    """
    mocker.patch('blurwal.environment.BACKENDS', {
        'feh': (create_autospec(base.Backend), 'feh'),
        'nitrogen': (create_autospec(base.Backend), 'nitrogen'),
        'xfce': (create_autospec(base.Backend), 'xfconf-query')
    })

    mocker.patch('blurwal.environment.XDG_TO_BACKEND', {'XFCE': 'xfce'})


def test_get_backend_returns_backend_for_given_name(mocker):
    mocker.patch('shutil.which', return_value='/usr/bin/feh')
    assert environment.get_backend('feh') == environment.BACKENDS['feh'][0]()


def test_get_backend_returns_backend_for_xdg_value(mocker, monkeypatch):
    monkeypatch.setenv('XDG_CURRENT_DESKTOP', 'XFCE')
    mocker.patch('shutil.which', return_value='/usr/bin/xfconf-query')
    assert environment.get_backend(None) == environment.BACKENDS['xfce'][0]()


def test_get_backend_falls_back_when_given_not_available(mocker, monkeypatch):
    monkeypatch.setenv('XDG_CURRENT_DESKTOP', 'XFCE')
    mocker.patch('shutil.which', return_value=None)
    assert environment.get_backend('feh') == environment.BACKENDS['xfce'][0]()


def test_get_backend_falls_back_to_first_available(mocker, monkeypatch):
    monkeypatch.setenv('XDG_CURRENT_DESKTOP', 'unknown')
    mocker.patch('shutil.which', return_value='/usr/bin/feh')
    assert environment.get_backend(None) == environment.BACKENDS['feh'][0]()


def test_get_backend_returns_none_when_none_available(mocker, monkeypatch):
    monkeypatch.setenv('XDG_CURRENT_DESKTOP', 'unknown')
    mocker.patch('shutil.which', return_value=None)
    assert environment.get_backend(None) is None


def test_prepare_creates_directories(mocker, tmp_path):
    backend_mock = create_autospec(base.Backend)

    # Should create blurwal/ subdirectory in ~/.cache
    mocker.patch('blurwal.paths.CACHE_DIR', tmp_path / '.cache/blurwal')
    # Should create blurwal/ subdirectory in temp directory
    mocker.patch('blurwal.paths.TEMP_DIR', tmp_path / 'blurwal')

    environment.prepare(backend_mock)

    assert paths.CACHE_DIR.is_dir()
    assert paths.TEMP_DIR.is_dir()
