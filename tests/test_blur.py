"""
Test cases for the main event listener.

Author: Benedikt Vollmerhaus
License: MIT
"""

from argparse import Namespace
from multiprocessing import pool
from unittest.mock import create_autospec

import pytest

from blurwal import frame, paths
from blurwal.backends import base
from blurwal.blur import Blur
from blurwal.transition import Transition


def test_init_transition_blurs_when_over_threshold(mocker, backend_mock):
    args = Namespace(min=2, steps=10, blur=0, ignore=[])
    blur = Blur(args, backend_mock)

    blur_thread = Transition(0, 0, backend_mock)
    unblur_thread = Transition(0, 0, backend_mock)

    transition_mock = mocker.patch('blurwal.blur.Transition')
    blur.init_transition(2, blur_thread, unblur_thread)
    transition_mock.assert_called_once_with(0, 10, backend_mock)


def test_init_transition_unblurs_when_under_threshold(mocker, backend_mock):
    args = Namespace(min=2, steps=10, blur=0, ignore=[])
    blur = Blur(args, backend_mock)

    blur_thread = Transition(0, 0, backend_mock)
    blur_thread.current_level = 10  # Blur thread has completed
    unblur_thread = Transition(0, 0, backend_mock)

    transition_mock = mocker.patch('blurwal.blur.Transition')
    blur.init_transition(0, blur_thread, unblur_thread)
    transition_mock.assert_called_once_with(10, 0, backend_mock)


def test_init_transition_does_not_blur_consecutively(mocker, backend_mock):
    args = Namespace(min=2, steps=10, blur=0, ignore=[])
    blur = Blur(args, backend_mock)

    # Blur as previous transition
    blur_thread, unblur_thread = blur.init_transition(
        2, Transition(0, 0, backend_mock), Transition(0, 0, backend_mock))

    transition_mock = mocker.patch('blurwal.blur.Transition')

    # Should not blur again consecutively
    blur.init_transition(2, blur_thread, unblur_thread)
    transition_mock.assert_not_called()


def test_init_transition_does_not_unblur_consecutively(mocker, backend_mock):
    args = Namespace(min=2, steps=10, blur=0, ignore=[])
    blur = Blur(args, backend_mock)

    # Unblur as previous transition
    blur_thread, unblur_thread = blur.init_transition(
        0, Transition(0, 0, backend_mock), Transition(0, 0, backend_mock))

    transition_mock = mocker.patch('blurwal.blur.Transition')

    # Should not unblur again consecutively
    blur.init_transition(0, blur_thread, unblur_thread)
    transition_mock.assert_not_called()


def test_frames_are_outdated(mocker, shared_datadir, backend_mock):
    mocker.patch('blurwal.paths.CACHE_DIR', shared_datadir / 'cache_dir')
    mocker.patch('blurwal.frame.is_outdated', return_value=False)

    args = Namespace(steps=10, blur=0, min=0, ignore=[])
    blur = Blur(args, backend_mock)
    assert not blur.frames_are_outdated()


def test_frames_are_outdated_when_frame_missing(mocker, shared_datadir,
                                                backend_mock):
    mocker.patch('blurwal.paths.CACHE_DIR', shared_datadir / 'cache_dir')
    args = Namespace(steps=11, blur=0, min=0, ignore=[])
    blur = Blur(args, backend_mock)
    assert blur.frames_are_outdated()


def test_frames_are_outdated_when_frame_outdated(mocker, shared_datadir,
                                                 backend_mock):
    mocker.patch('blurwal.paths.CACHE_DIR', shared_datadir / 'cache_dir')
    mocker.patch('blurwal.frame.is_outdated', return_value=True)

    args = Namespace(steps=10, blur=0, min=0, ignore=[])
    blur = Blur(args, backend_mock)
    assert blur.frames_are_outdated()


def test_generate_transition_frames(mocker, backend_mock):
    mocker.patch('blurwal.utils.show_notification')
    starmap_mock = mocker.patch.object(pool.Pool, 'starmap')

    args = Namespace(steps=10, blur=8.5, min=0, ignore=[])
    blur = Blur(args, backend_mock)

    expected_jobs = [(paths.CACHE_DIR, l, 10, 8.5) for l in range(11)]

    blur.generate_transition_frames()
    starmap_mock.assert_called_once_with(frame.generate, expected_jobs)


@pytest.fixture(scope='module')
def backend_mock():
    return create_autospec(base.Backend)
