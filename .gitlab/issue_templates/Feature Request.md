<!--
  Before requesting a feature, check that it hasn't already been requested:
    ↪ https://gitlab.com/BVollmerhaus/blurwal/issues?label_name[]=Feature

  If you found an existing request, simply upvote it to show your interest.
-->

## Summary
<!-- Concisely summarize the feature you'd like to see. -->


## Attachments
<!--
  If practical: Additional information that may help convey
  your idea, such as mockups, references, screenshots, etc.
-->

–


/label ~Feature
